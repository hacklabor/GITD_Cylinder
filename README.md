# Projekt Glown in the Dark Zylinder
 * Grundidee des Projektes Eigenbaukombinat[https://eigenbaukombinat.de/](https://eigenbaukombinat.de/)
 * Projektpage [https://hackaday.io/project/11868-glowboard-plotter](https://hackaday.io/project/11868-glowboard-plotter)


## Erste Entwürfe

![](/Bilder/Ansicht_vorn.PNG)

![](/Bilder/Ansicht_hinten.PNG)

![](/Bilder/Ansicht_oben.PNG)

* 32 LEDs 3mm UV 60 mW  Abstand 3,6mm [https://www.led1.de/shop/led-3mm-uv-schwarzlicht-60mw-webuv02-cs.html](https://www.led1.de/shop/led-3mm-uv-schwarzlicht-60mw-webuv02-cs.html)
* Auflösung 102*32 Dots Abstand 4x4 mm
* wer Zugriff auf die Fusion 360 Daten haben will meldet sich bei mir
* Größe Zylinder Durchmesser 130mm höhe 130mm 
* Übersetzung 60/20 = 0,6 Grad je Step = 0,010472 RAD * 55mm = 0,57596mm auf der Außenseite
* led Ansteuerung [http://howtomechatronics.com/tutorials/arduino/how-to-extend-arduino-pwm-outputs-tlc5940-tutorial/](http://howtomechatronics.com/tutorials/arduino/how-to-extend-arduino-pwm-outputs-tlc5940-tutorial/)

## Elektroplan

![](/Bilder/GitdZylinder_Steckplatine.png)

## Einkaufsliste

* 32x LED 3mm UV 60mW [https://www.led1.de/shop/led-3mm-uv-schwarzlicht-60mw-webuv02-cs.html](https://www.led1.de/shop/led-3mm-uv-schwarzlicht-60mw-webuv02-cs.html) 
* 32x Widerstand 150 Ohm SMD 0805 zum Parrallellöten auf dem Adafruitboard
* 2x Adafruit PCA9685 16 Kanal 12-Bit PWM Servomotor Treiber IIC Modul [https://www.amazon.de/gp/product/B06XSFFXQY/ref=oh_aui_detailpage_o04_s00?ie=UTF8&psc=1](https://www.amazon.de/gp/product/B06XSFFXQY/ref=oh_aui_detailpage_o04_s00?ie=UTF8&psc=1) 
* 1x DRV8825 Treiber für Stepper Motor [https://www.amazon.de/gp/product/B06Y6536CB/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1](https://www.amazon.de/gp/product/B06Y6536CB/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)
* 1x Raspi Zero W [https://www.amazon.de/gp/product/B0727YF3KP/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.de/gp/product/B0727YF3KP/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)
* 1x Zahriemenscheibe GT2 5mm 20 Zähne [https://www.amazon.de/gp/product/B06XF3TMKJ/ref=oh_aui_detailpage_o08_s00?ie=UTF8&psc=1](https://www.amazon.de/gp/product/B06XF3TMKJ/ref=oh_aui_detailpage_o08_s00?ie=UTF8&psc=1) 
* 1x Zahnriemscheibe GT2 8mm 60 Zähne [https://www.amazon.de/gp/product/B07115W296/ref=oh_aui_detailpage_o02_s01?ie=UTF8&psc=1](https://www.amazon.de/gp/product/B07115W296/ref=oh_aui_detailpage_o02_s01?ie=UTF8&psc=1)
* 1x Zahnriemen GT2 288mm [https://www.amazon.de/gp/product/B0757KQGSR/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1](https://www.amazon.de/gp/product/B0757KQGSR/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1)
* 1x Stepper Motor NEMA17 [https://www.amazon.de/gp/product/B015ST2U5A/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1](https://www.amazon.de/gp/product/B015ST2U5A/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1)
* 1x Leifheit 31200 Vorratsbehälter Fresh&Easy 900 ml [https://www.amazon.de/gp/product/B003AM896C/ref=oh_aui_detailpage_o06_s00?ie=UTF8&psc=1](https://www.amazon.de/gp/product/B003AM896C/ref=oh_aui_detailpage_o06_s00?ie=UTF8&psc=1)
* 1x lumentics Premium Leuchtfolie - Im Dunkeln nachleuchtende Bastelfolie, zuschneidbare Nachleuchtfolie, Profi Leuchtpapier, DIY Wandtattoo, Bastelpapier, Wandsticker, Schwarzlichtfolie (A4, Gelb-Grün) [https://www.amazon.de/gp/product/B01BSRKBBG/ref=oh_aui_detailpage_o08_s00?ie=UTF8&psc=1](https://www.amazon.de/gp/product/B01BSRKBBG/ref=oh_aui_detailpage_o08_s00?ie=UTF8&psc=1)
* 1x 2Stk sourcingmap® 2Stück KFL08 8mm Schaft vertikal Selbstausrichtungs Lagerblock Flanschlager [https://www.amazon.de/gp/product/B01LX5OGCO/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1](https://www.amazon.de/gp/product/B01LX5OGCO/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1)
