#!/usr/bin/env python
"""
================================================
ABElectronics Servo Pi pwm controller | PWM output demo

run with: python demo_pwm.py
================================================

This demo shows how to set a 1KHz output frequency and change the pulse width
between the minimum and maximum values
"""

from __future__ import absolute_import, division, print_function, \
                                                    unicode_literals

try:
    from ServoPi import PWM
except ImportError:
    print("Failed to import IOPi from python system path")
    print("Importing from parent folder instead")
    try:
        import sys
        sys.path.append("..")
        from ServoPi import PWM
    except ImportError:
        raise ImportError(
            "Failed to import library from parent folder")

from PIL import Image
import time

def main():
    """
    Main program function
    """
    print ('test')
    # create an instance of the PWM class on i2c address 0x40
    pwm = PWM(0x40)

    # Set PWM frequency to 1 Khz and enable the output
    pwm.set_pwm_freq(1000)
    pwm.output_enable()
 
    while True:
        
        #img = Image.new( 'L', (7,8), "grey") # create a new black image
        img = Image.open('test.bmp')
        pixels = img.load() # create the pixel map

        for i in range(img.size[0]):    # for every col:
          
            #time.sleep(1)
            #pwm.set_all_pwm(0,4095)
            time.sleep(0.1)
            
            for j in range(img.size[1]):    # For every row
                # pixels[i,j] = (i, j, 100) # set the colour accordingly
                print (str(i) +'; '+ str(j) +'; '+ str(pixels[i,j]*16+15)+chr(13))
               
                pwm.set_pwm(j+1, 0, pixels[i,j]*16+15)
      

if __name__ == "__main__":
    main()